# Andrew Mead Courses 

[Website] (https://mead.io/)

## Basics JSX for React

### Installing env
* ~~npm install -g babel-cli@6.24.1~~
* yarn init (enter all) 
* npm add live-server babel-cli@6.24.1
* npm add babel-preset-react@6.24.1 babel-preset-env@1.5.2
* npm add webpack@3.1.0
* npm add react@17.0.2
* npm add react-dom@17.0.2
* npm add babel-core@6.25.0 babel-loader@7.1.1
* npm add webpack-dev-server@2.5.1
* npm add babel-plugin-transform-class-properties@6.24.1
* npm add react-modal
* npm add style-loader@0.18.2 css-loader@0.28.4
* npm add sass-loader@6.0.6 node-sass@4.5.3 
* npm add normalize.css@7.0.0
* npm add react-router-dom@5.2.0

### Run the JSX using Babel
* ~~babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch~~
* ~~live-server public~~
* npm run dev-server