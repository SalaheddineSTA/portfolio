//-----------------
//app.js
//-----------------

import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './Routers/AppRouter'

import 'normalize.css/normalize.css';
import './Styles/styles.scss';


const appRoot = document.getElementById('app');
ReactDOM.render(<AppRouter/>,appRoot);
