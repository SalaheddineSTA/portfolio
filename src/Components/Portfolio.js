import React from 'react';
import {Link} from 'react-router-dom';

const Portfolio = (props) => {
    return(
        <div>
            <h1>My works</h1>
            <ul>
                <li><Link to="/portfolio/1">Work 1</Link></li>
                <li><Link to="/portfolio/2">Work 2</Link></li>
                <li><Link to="/portfolio/3">Work 3</Link></li>
            </ul>
        </div>
    );
};

export default Portfolio;