import React from 'react';
import {NavLink} from 'react-router-dom';

const Header = () => (
    <header>
        <h1>S.<br/>STA</h1>
        <ul>
            <li><NavLink to="/" activeClassName="is-active"  className="nav-link" exact={true}>   About Me   </NavLink></li>
            <li><NavLink to="/resume" activeClassName="is-active" className="nav-link" >          Resume    </NavLink></li>
            <li><NavLink to="/portfolio" activeClassName="is-active" className="nav-link" >       Portfolio    </NavLink></li>
            <li><NavLink to="/blog" activeClassName="is-active" className="nav-link" >            Blog    </NavLink></li>
            <li><NavLink to="/contact" activeClassName="is-active" className="nav-link" >         Contact   </NavLink></li>
        </ul>
    </header>
);

export default Header;