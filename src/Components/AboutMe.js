import React from 'react';

const AboutMe = () => (
    // <div>
    //     <div className="about-me__title">About Me</div>
    //     <div className="container__about-me">
    //         <div className="container__about-me__image">
    //         </div>
    //         <div className="container__about-me__body">
    //             <h1>Salaheddine STA</h1>
    //             <h2>PhD____Developer</h2>
    //             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis elit ut enim pharetra bibendum. Donec euismod aliquet nibh eu facilisis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis in ante sed nunc euismod faucibus. Pellentesque tortor nibh, vestibulum ac dignissim vitae, semper eu massa. Phasellus tempus sapien in enim aliquet hendrerit. Ut et feugiat arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
    //         </div>
    //     </div>
    // </div>

    <div className="container__about-me">
            {/* <div className="about-me__title">Title</div> */}
            <div className="container__about-me__image">
            </div>
            <div className="container__about-me__body">
                <h1>Salaheddine STA</h1>
                <h2>PhD____Developer</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis elit ut enim pharetra bibendum. Donec euismod aliquet nibh eu facilisis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis in ante sed nunc euismod faucibus. Pellentesque tortor nibh, vestibulum ac dignissim vitae, semper eu massa. Phasellus tempus sapien in enim aliquet hendrerit. Ut et feugiat arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
            </div>
        </div>
);

export default AboutMe;