
import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import Header from '../Components/Header'
import AboutMe from '../Components/AboutMe'
import Resume from '../Components/Resume'
import Portfolio from '../Components/Portfolio'
import PortfolioItem from '../Components/PortfolioItemPage'
import Blog from '../Components/Blog'
import BlogItem from '../Components/BlogItemPage'
import Contact from '../Components/Contact'
import NotFoundPage from '../Components/NotFoundPage'

const AppRouter = () => (
    <BrowserRouter>
    <div className="container">
        <div className="container__header">
            <Header  /> 
        </div>
        <div className = "container__body">
        <Switch>
            <Route 
                path="/" 
                component={AboutMe} 
                exact={true}
            />
            <Route 
                path="/resume" 
                component={Resume} 
            />

            <Route 
                path="/portfolio" 
                component={Portfolio} 
                exact={true}
            /> 
            <Route 
                path="/portfolio/:id" 
                component={PortfolioItem} 
            />
            <Route 
                path="/blog" 
                component={Blog} 
                exact={true}
            />
            <Route 
                path="/blog/:id" 
                component={BlogItem} 
            />
            <Route 
                path="/contact" 
                component={Contact} 
            />
            <Route 
                component={NotFoundPage} 
            />
            
        </Switch>
        </div>
        
        
    </div>
    </BrowserRouter>
);

export default AppRouter;