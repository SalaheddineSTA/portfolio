const path = require('path');

module.exports = {
    entry: './src/app.js',
    output:{
        path: path.join(__dirname,'public'),
        filename:'bundle.js'
    },
    module:{
        //js, react 
        rules:[{
            loader:'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        },
        //css
        {
            test:/\.s?css$/,
            use:[
                'style-loader',
                'css-loader?url=true',
                'sass-loader'
            ]
        }]
    },
    //devtool: 'cheap-module-eval-source-map', // for debugin
    devServer:{
        contentBase: path.join(__dirname,'public'),
        historyApiFallback: true
    }
};

//loader
